# Install Guile

## Installation

### On GNU/Linux

Installing Guile is easy as pie via a package manager.

**Using Guix (recommended)**

```bash
$ guix install guile
```

**Using apt (on Debian and derivatives)**

when based on stable

```bash
$ apt install guile-2.0
```

when based on testing/stable

```bash
$ apt install guile-3.0
```

**Using Yum (RedHat based systems )**

```bash
$ yum install guile
```

**Using Pacman (ArchLinux based systems)**

```bash
$ pacman -S guile
```

### On MacOS

A procedure for MacOS is available [here](https://github.com/aconchillo/homebrew-guile).

### On Windows

No solution yet.

## Configuration

Guile comes with a tool commonly called a REPL (Read-Eval-Print-Loop), a kind of super interpreter. But without configuration, it remains very rudimentary and is not very practical.

![repldown](images/guile-brut.gif)

Nothing very complicated but you have to activate two modules (one of which you have to install manually).

```bash
$ guix package -i guile-colorized
```

If you don't use Guix, you can find the installation instructions for this module [here](https://gitlab.com/NalaGinrut/guile-colorized).

Then, you can configure your interpreter by modifying the `~/.guile` file (create it if it doesn't exist) and put the following lines into it:

```scheme
(use-modules (ice-9 readline)
             (ice-9 colorized))

(activate-readline)
(activate-colorized)
```

With this your REPL is now more colorful, has a history of past evaluations in which you can navigate and a visual aid for closing a brackets.

![replup](images/guile-net.gif)


## Tooling

### Editor

The editor is a very personal choice. However, [Emacs](https://www.gnu.org/software/emacs/) remains the editor that provides the best support for Guile.

You can install it in the same way as you install Guile (below, example with Guix):

```bash
$ guix install emacs
```
